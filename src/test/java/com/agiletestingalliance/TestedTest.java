package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.*;
 
public class TestedTest {

    @Test
    public void testGstr() {
        Tested tested = new Tested("Hello, World!");
        String expected = "Hello, World!";
        String actual = tested.gstr();
        assertEquals(expected, actual);
    }
}
