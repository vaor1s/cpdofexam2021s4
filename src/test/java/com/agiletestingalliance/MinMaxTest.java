package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {
    @Test
    public void testnumberf() {
        MinMax minMax = new MinMax();
        assertEquals(5, minMax.numberf(3, 5));
        assertEquals(10, minMax.numberf(10, 7));
        assertEquals(-1, minMax.numberf(-1, -5));
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals("hello", minMax.bar("hello"));
        assertEquals("", minMax.bar(""));
        assertNull(minMax.bar(null));
    }
}
